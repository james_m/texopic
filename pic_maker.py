#! /usr/bin/python3
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw 
import rtl
class picture_maker:
    def __init__(self):
        self.text=1
    def make_pic(text="your text!",pic="f_nvidia.jpg",rgb=(0,0,0),coordinates=(0,0),size=30):
        font="Vazir.ttf"
        if len(text)>30:
            
            #Separate the words by ' ' .
            temp_text=text.split(' ')
            text=''
            n=0
            line=0
            lines=['']
            #paste the words together in different lines but each line cannot be over 35 letters.
            while n!=len(temp_text):
                lines[line]+=temp_text[n]+' '
                if len(lines[line])>=30:
                    lines[line]+='\n'
                    line+=1
                    lines.append('')    
                n+=1
            #Paste the lines together
            if not '\n' in lines[line]: lines[line]+='\n'
            i=0
            for _line in lines:
                text+=_line
                i+=1
                if i==6:break
        img = Image.open("pics/"+pic)
        draw = ImageDraw.Draw(img)
        font = ImageFont.truetype(font,size)
        draw.text(coordinates,rtl.rtl(text),rgb,font=font,align='center')
        img.save('pics/temp.jpg')


