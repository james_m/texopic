# Texopic
A telegram bot to write text on pictures

## Dependencies 
```
- telepot,rtl.PIL
$ sudo -H pip3 install telepot
$ sudo -H pip3 install rtl
$ sudo -H pip3 install Pillow

```
## Usage
```
$ git clone https://gitlab.com/james_m/texopic
$ cd texopic
$ python3 bot.py
```
or:
```
$ git clone https://gitlab.com/james_m/texopic;cd texopic;python3 bot.py;
```
## License
This project is licensed under the GNU General Public License v3.0 - see the [LICENSE.md](LICENSE.md) file for details