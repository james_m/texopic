#! /usr/bin/python3
# -*- coding: utf-8 -*-
import time
import random
import datetime
import telepot
from telepot.loop import MessageLoop
from pic_maker import picture_maker
def handle(msg):
    mpic=picture_maker
    print(msg)
    chat_id = msg['chat']['id']
    command = msg['text']
    msg_id = int(msg['message_id'])
    first_name=msg['from']['first_name']
    if 'last_name' in msg['from'].keys():
        last_name=msg['from']['last_name']
    else:
        last_name=''
    if 'username' in msg['from'].keys():
        username=msg['from']['username']
    else:
        username=''
    if 'reply_to_message' in msg.keys():
        isreply=True
        reply_text=msg['reply_to_message']['text']
        reply_msg_id=msg['reply_to_message']['message_id']
        reply_first_name=msg['reply_to_message']['from']['first_name']
        if 'last_name' in msg['reply_to_message']['from'].keys():
            reply_last_name=msg['reply_to_message']['from']['last_name']
        else:
            reply_last_name=''
        if 'username' in msg['reply_to_message']['from'].keys():
            reply_username=msg['reply_to_message']['from']['username']
        else:
            reply_username=''
    else:
        isreply=False
    print('{name} ({username}) sent command: {cmd}'.format(name=first_name+' '+last_name,username=username,cmd=command))
    if '/pic' in command :
        if len(command)<5 :
            bot.sendMessage(chat_id,"You have to put something infront of /pic ")
        else:
            mpic.make_pic(command[4:],"niche.jpg",(255,255,255),(40,50),25)
            mpic.make_pic("-- "+first_name+' '+last_name,"temp.jpg",(255,255,255),(200,300),18)
            bot.sendPhoto(chat_id,open('./pics/temp.jpg', 'rb'),command[4:],reply_to_message_id=msg_id)
    elif command == '/start':
        bot.sendMessage(chat_id, "usage: /pic text")
    elif command == '/suckit':
        if isreply:    
            mpic.make_pic(reply_text,"niche.jpg",(255,255,255),(40,50),28)
            mpic.make_pic("-- "+reply_first_name+' '+reply_last_name,"temp.jpg",(255,255,255),(200,300),18)
            bot.sendPhoto(chat_id,open('./pics/temp.jpg', 'rb'),reply_to_message_id=reply_msg_id)
        else:
            bot.sendMessage(chat_id,"You need to reply this to a fucking message honey!",reply_to_message_id=msg_id)
bot = telepot.Bot('------TOKEN------')
MessageLoop(bot, handle).run_as_thread()
print('I am listening ...')
    
while 1:
   time.sleep(10)
